FROM moodlehq/moodle-php-apache:8.1-buster
RUN docker-php-ext-install pcntl
RUN docker-php-ext-install posix
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN composer
ENV NODE_VERSION=16.13.0
RUN apt install -y curl
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN composer create-project moodlehq/moodle-plugin-ci /var/www/ci ^4
RUN chmod 777 /var/www/ci/bin/moodle-plugin-ci
ENV PATH="/var/www/ci/bin:${PATH}"
RUN composer
RUN moodle-plugin-ci -v